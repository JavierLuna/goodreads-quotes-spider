## Extract test

## How to set up

Inside a virtual environment install dependencies found in `requirements.txt` like this:

``pip install -r requirements.txt``

Run the solution like `scrapy runspider goodreads.py`. It will dump the results in a file called `goodread_fashion.csv`.
It will also download all images in the `images` folder.