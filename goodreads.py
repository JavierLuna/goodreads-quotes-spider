# -*- coding: utf-8 -*-
import scrapy


class GoodreadsSpider(scrapy.Spider):
    name = 'goodreads'
    start_urls = ['https://www.goodreads.com/quotes/tag/fashion/']

    custom_settings = {
        "ITEM_PIPELINES": {'scrapy.contrib.pipeline.images.ImagesPipeline': 1},
        "IMAGES_STORE": "./images",
        "FEED_FORMAT": "csv",
        "FEED_URI": "./goodread_fashion.csv"
    }

    def parse(self, response):
        quote_details = response.css(".quoteDetails")
        for quote_detail in quote_details:
            img_url = quote_detail.css("img::attr(src)").extract_first()
            quote_text = quote_detail.css(".quoteText::text").extract_first().strip()
            author = quote_detail.css(".authorOrTitle::text").extract_first().split(",")[0].strip()
            tags = quote_detail.css(".quoteFooter").css("a::text").extract()[:-1]

            yield {"img_url": img_url,
                   "quote_text": quote_text,
                   "author": author,
                   "tags": tags,
                   "image_urls": [img_url] if img_url else []}

        next_page = response.css(".next_page::attr(href)").extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
